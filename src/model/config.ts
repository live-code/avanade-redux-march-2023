export type Theme =  'dark' | 'light';
export type Language = 'it' | 'en';

export interface ConfigState {
  theme: Theme;
  language: Language;
}

