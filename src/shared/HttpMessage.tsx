import * as classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../App.tsx';
import { setHttpStatus } from '../core/store/http-status.store.ts';

export function HttpMessage() {
  const httpStatus = useSelector((state: RootState) => state.httpStatus)
  const dispatch = useDispatch();

  return httpStatus.status ? (
    <div>
      <div
        className={classNames('alert', {
          'alert-danger': httpStatus.status === 'error',
          'alert-success': httpStatus.status === 'success',
        })}>
        {httpStatus.msg}

        <button onClick={() => {
          dispatch(setHttpStatus({ status: null, msg: null}))
        }}>close</button>
      </div>
    </div>
  ) : null
}
