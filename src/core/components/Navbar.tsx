import * as cn from 'classnames';
import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { RootState } from '../../App.tsx';
import { usePrefetch } from '../../features/users/store/users.api.ts';

const activeStyle: React.CSSProperties = { color: 'orange'};

export const Navbar = () => {
  const theme = useSelector((state: RootState) => state.config.theme)
  const lang = useSelector((state: RootState) => state.config.language)

  const prefetchUsers = usePrefetch('getUsers')

  return (
    <nav className={cn(
      'navbar navbar-expand',
      { 'navbar-dark bg-dark': theme === 'dark' },
      { 'navbar-light bg-light': theme === 'light' },
    )}>
      <div className="navbar-brand">
        <NavLink
          style={(obj) => obj.isActive ? activeStyle : {}}
          className="nav-link"
          to="/">
          REDUX {lang === 'it' ? '🇮🇹' : '🇬🇧'}
        </NavLink>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink style={(obj) => obj.isActive ? activeStyle  : {} }
                     className="nav-link"
                     to="/settings">
              <small>settings</small>
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/counter">
              <small>counter</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/users"
              onMouseOver={() => prefetchUsers()}
            >
              <small>users</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/catalog">
              <small>catalog</small>
            </NavLink>
          </li>

        </ul>
      </div>
    </nav>
  )
}
