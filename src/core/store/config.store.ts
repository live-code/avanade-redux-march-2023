import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ConfigState, Language, Theme } from '../../model/config.ts';

const initialState: ConfigState = {
  theme: 'dark',
  language: 'it',
}

export const configStore = createSlice({
  name: 'config',
  initialState,
  reducers: {
    changeTheme(state, action: PayloadAction<Theme>) {
      state.theme = action.payload;
    },
    changeLanguage(state, action: PayloadAction<Language>) {
      state.language = action.payload;
    },
  }
})

export const {
  changeLanguage,
  changeTheme
} = configStore.actions
