// core/store/http-status.store.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface HttpStatus {
  status: 'error' | 'success' | null,
  msg: string | null
}

export const httpStatusStore = createSlice({
  name: 'httpStatus',
  initialState: { status: null, msg: null } as HttpStatus,
  reducers: {
    setHttpStatus(state, action: PayloadAction<HttpStatus>) {
      state.status = action.payload.status;
      state.msg = action.payload.msg;
    }
  }
})

export const {
  setHttpStatus
} = httpStatusStore.actions;
