import { HttpStatusCode } from 'axios';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { RootState, useAppDispatch } from '../../App.tsx';
import { HttpMessage } from '../../shared/HttpMessage.tsx';
import { addProduct, deleteProduct, getProducts } from './store/products.actions.ts';

export const CatalogPage = () => {
  const dispatch = useAppDispatch();
  const products = useSelector((state: RootState) => state.catalog.list)
  const pending = useSelector((state: RootState) => state.catalog.pending)

  useEffect(() => {
    dispatch(getProducts())
  }, [dispatch])


  function addHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addProduct({
        title: e.currentTarget.value,
        price: 10
      }))
    }
  }

  function deleteHandler(id: number) {
    dispatch(deleteProduct(id))
  }

  return <div>
    CatalogPage

    {pending && <div className="alert alert-info">LOADING...</div>}
    <HttpMessage />

    <input type="text" onKeyDown={addHandler}/>

    {
      products.map(p => {
        return (
          <li key={p.id}>
            {p.title}
            <i className="fa fa-trash"
               onClick={() => deleteHandler(p.id)}></i>
          </li>
        )
      })
    }
  </div>
};
