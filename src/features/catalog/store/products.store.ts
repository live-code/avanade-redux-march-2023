import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Product } from '../../../model/product.ts';
import { addProductsStart, getProductsStart } from './products.actions.ts';

interface ProductState {
  list: Product[];
  error: string | null;
  pending: boolean
}


const initialState: ProductState = {
  list: [],
  error: null,
  pending: false
}

export const productsStore = createSlice({
  name: 'products',
  initialState,
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      state.list =  action.payload
      state.pending = false;
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.list.push(action.payload)
      state.pending = false;
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      state.error = null;
      state.pending = false;
      const index = state.list.findIndex(p => p.id === action.payload)
      state.list.splice(index, 1)
      // return state.filter(p => p.id !== action.payload)
    },
    getProductsFailed(state) {
      state.error = 'Get product failed!'
    },
    setError(state, action: PayloadAction<string | null>) {
      state.error = action.payload;
    }
  },
  extraReducers: builder => builder
    .addCase(getProductsStart, (state) => {
      state.error = null;
      state.pending = true;
    })
    .addCase(addProductsStart, (state) => {
      state.error = null;
      state.pending = true;
    })
})

export const  {
  setError,
  getProductsFailed,
  getProductsSuccess,
  addProductSuccess,
  deleteProductSuccess
} = productsStore.actions
