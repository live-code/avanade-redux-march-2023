import { createAction } from '@reduxjs/toolkit';
import axios from 'axios';
import { AppThunk } from '../../../App.tsx';
import { setHttpStatus } from '../../../core/store/http-status.store.ts';
import { Product } from '../../../model/product.ts';
import {
  addProductSuccess,
  deleteProductSuccess,
  getProductsFailed,
  getProductsSuccess,
  setError
} from './products.store.ts';


export const getProductsStart = createAction('products/getProductsStart')
export const addProductsStart = createAction<Partial<Product>>('products/addProductsStart')



export const getProducts = (): AppThunk => async dispatch => {
  dispatch(getProductsStart())
  try {
    const res = await axios.get<Product[]>('http://localhost:3000/products')
    dispatch(getProductsSuccess(res.data))
  } catch (e) {
    dispatch(setHttpStatus({ status: 'error', msg: 'AHIA! GET FALLITA!'}))
  }

}

export function getProducts2(): AppThunk {
  return async function (dispatch) {
    const res = await axios.get<Product[]>('http://localhost:3000/products')
    dispatch(getProductsSuccess(res.data))
  }
}

export function getProducts3(): AppThunk {
  return function (dispatch) {
    axios.get<Product[]>('http://localhost:3000/products')
      .then(res => {
        dispatch(getProductsSuccess(res.data))
      })

  }
}


export const addProduct = (product: Partial<Product>): AppThunk => (dispatch) => {
  dispatch(addProductsStart(product))
  const params: Partial<Product> = {
    ...product,
    visibility: false
  }
  axios.post<Product>('http://localhost:3000/products', params)
    .then(res => {
      dispatch(addProductSuccess(res.data))
      dispatch(setHttpStatus({ status: 'success', msg: 'Hai inserito i dati con successo'}))
    })
}


export const deleteProduct = (id: number): AppThunk => async (dispatch) => {
  try {
    await axios.delete(`http://localhost:3000/products/${id}`);
    dispatch(deleteProductSuccess(id))
  } catch (err) {
    // dispatch error
  }
}
