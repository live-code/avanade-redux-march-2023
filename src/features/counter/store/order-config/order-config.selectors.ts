import { RootState } from '../../../../App.tsx';


export const selectMaterial = (state: RootState) => state.orders.orderConfig.material
export const selectItemsPerPallet = (state: RootState) => state.orders.orderConfig.itemsPerPallet
