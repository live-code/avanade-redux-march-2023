import { createReducer } from '@reduxjs/toolkit';
import { updateMaterial, updatePalletTotal } from './order-config.actions.ts';

const initialState = {
  itemsPerPallet: 5,
  material: 'wood'
};

export const orderConfigReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(updatePalletTotal, (state, action) => {
      state.itemsPerPallet = action.payload
    })
    .addCase(updateMaterial, (state, action) => {
      state.material = action.payload
    })
)
