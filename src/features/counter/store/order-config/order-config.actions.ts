import { createAction } from '@reduxjs/toolkit';

export const updateMaterial = createAction<'wood' | 'plastic'>('update material')
export const updatePalletTotal = createAction<number>('update pallet total')
