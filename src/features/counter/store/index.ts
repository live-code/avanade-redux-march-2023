import { combineReducers } from '@reduxjs/toolkit';
import { counterReducer } from './counter/counter.reducer.ts';
import { orderConfigReducer } from './order-config/order-config.reducer.ts';

export const ordersReducers = combineReducers({
  counter: counterReducer,
  orderConfig: orderConfigReducer,
})
