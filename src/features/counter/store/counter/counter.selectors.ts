import { RootState } from '../../../../App.tsx';


export const selectCounter = (state: RootState) => state.orders.counter.value

export const selectTotalPallets = (state: RootState) =>
  Math.ceil(state.orders.counter.value / state.orders.orderConfig.itemsPerPallet)
