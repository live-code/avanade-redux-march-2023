import { createReducer } from '@reduxjs/toolkit';
import { decrement, increment, reset } from './counter.actions.ts';

const initialState = {
  value: 0,
};

export const counterReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(increment, (state, action) => ({...state, value: state.value + action.payload } ) )
    .addCase(decrement, (state, action) => {
      state.value -= action.payload
    })
    .addCase(reset, () => initialState)
)


/*
export const counterReducer = createReducer(0, {
  [increment.type]: (state, action: PayloadAction<number>) => state + action.payload,
  [decrement.type]: (state, action: PayloadAction<number>) => state - action.payload
})
*/

/*
export const counterReducer = createReducer(0, {
  [increment.type]: (state, action) => {
    return state + action.payload
  },
  [decrement.type]: (state, action) => {
    return state - action.payload
  },
})*/

/*
export function counterReducer(state = 0, action: any) {
  console.log(state, action)
  switch (action.type) {
    case 'increment':
      return state + action.payload

    case 'decrement':
      return state - action.payload
  }
  return state;
}
*/
