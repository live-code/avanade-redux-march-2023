import { useDispatch, useSelector } from 'react-redux';
import { decrement, increment, reset } from './store/counter/counter.actions.ts';
import { selectCounter, selectTotalPallets } from './store/counter/counter.selectors.ts';
import { updateMaterial, updatePalletTotal } from './store/order-config/order-config.actions.ts';
import { selectItemsPerPallet, selectMaterial } from './store/order-config/order-config.selectors.ts';

export const CounterPage = () => {
  const dispatch = useDispatch()
  const counter = useSelector(selectCounter)
  const totalPallets = useSelector(selectTotalPallets)
  const material = useSelector(selectMaterial)
  const itemsPerPallet = useSelector(selectItemsPerPallet)

  function confirmHandler() {
    console.log(counter, totalPallets, material, itemsPerPallet)
  }

  return <div>

    <h1>Total Product {counter}</h1>
    <div>{totalPallets} pallets ({itemsPerPallet} items per pallet) of {material}</div>
    <button onClick={() => dispatch(decrement(5))}>-</button>
    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(reset())}>RESET</button>
    <hr/>
    <button onClick={() => dispatch(updateMaterial('wood'))}>WOOD</button>
    <button onClick={() => dispatch(updateMaterial('plastic'))}>PLASTIC</button>
    <button onClick={() => dispatch(updatePalletTotal(5))}>set pallet to 5</button>
    <button onClick={() => dispatch(updatePalletTotal(10))}>set pallet to 10</button>
    <hr/>
    <button onClick={confirmHandler}>CONFIRM</button>
  </div>
};
