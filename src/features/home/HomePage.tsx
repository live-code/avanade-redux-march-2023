import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App.tsx';
import { NewsList } from './components/NewsList.tsx';
import { addNews, deleteNews } from './store/news.store.ts';

export const HomePage = () => {
  const dispatch = useDispatch();
  const news = useSelector((state: RootState) => state.news);

  function addNewsHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addNews(e.currentTarget.value))
      e.currentTarget.value = ''
    }
  }

  function deleteHandler(id: number) {
    dispatch(deleteNews(id))
  }

  return <div>
    <input type="text" onKeyDown={addNewsHandler}/>

    <NewsList
      items={news}
      onDeleteItem={deleteHandler}
    />

  </div>
};
