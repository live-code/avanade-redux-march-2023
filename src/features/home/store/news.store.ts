import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { News } from '../../../model/news.ts';

const initialState: News[] = []

export const newsStore = createSlice({
  name: 'news',
  initialState,
  reducers: {
    addNews(state, action: PayloadAction<string>) {
      state.push({
        id: Date.now(),
        title: action.payload,
        published: false
      })
      // return [...state, news]
    },
    deleteNews(state, action: PayloadAction<number>) {
      const index = state.findIndex(news => news.id === action.payload)
      state.splice(index, 1)
      // return state.filter(news => news.id !== action.payload)
    },
    toggleNews(state, action: PayloadAction<number>) {

    }
  }
})

export const {
  addNews,
  deleteNews,
  toggleNews
} = newsStore.actions
