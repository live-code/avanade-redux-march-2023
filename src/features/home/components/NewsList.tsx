import { News } from '../../../model/news.ts';

interface NewsListProps{
  items: News[];
  onDeleteItem: (id: number) => void
}

export function NewsList(props: NewsListProps) {
  return (
    <div>
      {
        props.items.map(n => {
          return (
            <li key={n.id}>
              {n.title}

              <i
                className="fa fa-trash"
                onClick={() => props.onDeleteItem(n.id)}
              ></i>

            </li>
          )
        })
      }
    </div>

  )
}
