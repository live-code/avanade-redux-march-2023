import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { User } from '../../../model/user.ts';

export const usersAPI = createApi({
  reducerPath: 'users',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://jsonplaceholder.typicode.com/' }),
  endpoints: (builder) => ({
    getUsers: builder.query<Pick<User, 'name' | 'email'>[], void>({
      query: () => 'users',
      transformResponse: (res: User[]) => {
        return res?.map(u => ({ name: u.name, email: u.email}))
      }
    }),
    getUserById: builder.query<User, number>({
      query: (id) => `users/${id}`
    })
  }),
  keepUnusedDataFor: 2

})

export const { usePrefetch, useGetUserByIdQuery, useGetUsersQuery } = usersAPI;
