import { useGetUserByIdQuery, useGetUsersQuery } from './store/users.api.ts';

export function UsersPage () {
  const id = 2;

  const { data, isError, isFetching } = useGetUsersQuery()

  return <div>
    UsersPage

    {isError && <div>error...</div>}
    {isFetching && <div>loading...</div>}

    <pre>{JSON.stringify(data, null, 2)}</pre>
  </div>
}
