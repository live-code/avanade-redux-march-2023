import { useDispatch } from 'react-redux';
import { changeLanguage, changeTheme } from '../../core/store/config.store.ts';

export const SettingsPage = () => {
  const dispatch = useDispatch()

  return <div>
    Settings

    <hr/>
    <button onClick={() => dispatch(changeTheme('dark'))}>Dark</button>
    <button onClick={() => dispatch(changeTheme('light'))}>Light</button>
    <button onClick={() => dispatch(changeLanguage('it'))}>it</button>
    <button onClick={() => dispatch(changeLanguage('en'))}>en</button>
  </div>
};
