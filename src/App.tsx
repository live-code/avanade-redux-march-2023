import { AnyAction, combineReducers, configureStore, ThunkAction, ThunkDispatch } from '@reduxjs/toolkit';
import { Root } from 'react-dom/client';
import { Provider, useDispatch } from 'react-redux';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { Navbar } from './core/components/Navbar.tsx';
import { configStore } from './core/store/config.store.ts';
import { httpStatusStore } from './core/store/http-status.store.ts';
import { CatalogPage } from './features/catalog/CatalogPage.tsx';
import { productsStore } from './features/catalog/store/products.store.ts';
import { CounterPage } from './features/counter/CounterPage.tsx';
import { ordersReducers } from './features/counter/store';
import { HomePage } from './features/home/HomePage.tsx';
import { newsStore } from './features/home/store/news.store.ts';
import { SettingsPage } from './features/settings/SettingPage.tsx';
import { usersAPI } from './features/users/store/users.api.ts';
import { UsersPage } from './features/users/UsersPage.tsx';

const rootReducer =  combineReducers({
  [usersAPI.reducerPath]: usersAPI.reducer,
  httpStatus: httpStatusStore.reducer,
  orders: ordersReducers,
  config: configStore.reducer,
  news: newsStore.reducer,
  catalog: productsStore.reducer
})

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([
      usersAPI.middleware,
    ])

})

export type RootState = ReturnType<typeof rootReducer>
export type AppThunk = ThunkAction<void, RootState, null, AnyAction>
export type AppDispatch = ThunkDispatch<RootState, any, AnyAction>
export const useAppDispatch = () => useDispatch<AppDispatch>();

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/home" element={<HomePage />} />
          <Route path="/counter" element={<CounterPage />} />
          <Route path="/users" element={<UsersPage />} />
          <Route path="/catalog" element={<CatalogPage />} />
          <Route path="/settings" element={<SettingsPage />} />
          <Route
            path="*"
            element={
              <Navigate to='/home' />
            }
          />
        </Routes>
      </BrowserRouter>
    </Provider>
  )
}

export default App
